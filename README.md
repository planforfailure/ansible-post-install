# Ansible Role: Post Install Setup
A Debian based post install deployment that configures/installs the following
- ssh 
- postfix
- vim/apticron
- [logrotate](https://www.redhat.com/sysadmin/setting-logrotate)
- [node-exporter](https://prometheus.io/docs/guides/node-exporter/)

***
#### Setup
Ensure the prerequiste roles are installed via _requirements.yml._ Ansible [vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html) is used for the majority of the variables and not defined.


#### Role Variables
```
---
# Set hostname
hostname: foobar.com

# node-exporter
# Version
vers: 1.6.0 # https://github.com/prometheus/node_exporter/releases

# Prometheus URL
amd64_url: https://github.com/prometheus/node_exporter/releases/download/v{{ vers }}/node_exporter-{{ vers }}.linux-amd64.tar.gz
arm64_url: https://github.com/prometheus/node_exporter/releases/download/v{{ vers }}/node_exporter-{{ vers }}.linux-arm64.tar.gz
armv6_url: https://github.com/prometheus/node_exporter/releases/download/v{{ vers }}/node_exporter-{{ vers }}.linux-armv6.tar.gz
armv7_url: https://github.com/prometheus/node_exporter/releases/download/v{{ vers }}/node_exporter-{{ vers }}.linux-armv7.tar.gz

# Architecture
amd64_arch: amd64
arm64_arch: arm64
v6_arch:  armv6
v7_arch:  armv7
```

#### Dependencies
The role expects [ansible-image-base](https://gitlab.com/planforfailure/ansible-image-base) has been deployed.


#### Installation
Ensure `ansible-galaxy install -r requirements.yml -p roles` successfully deploys the supplemental roles, before you run.

```bash
$ ansible-playbook --extra-vars '@passwd.yml' deploy.yml -K
```

```yaml
---
- hosts: all
  become: yes
  gather_facts: yes
  roles:
    - roles/ansible-post-install
    - roles/etchosts
    - roles/ssh
    - roles/postfix
    - roles/vim-apticron
    - roles/node-exporter
    - roles/motd

  post_tasks:
  - name: Deploy /etc/hosts
    ansible.builtin.include: roles/etchosts/tasks/main.yml
    
  - name: Install/Configure ssh
    ansible.builtin.include: roles/ssh/tasks/main.yml

  - name: Install/Configure postfix
    ansible.builtin.include: roles/postfix/tasks/main.yml

  - name: Install/Configure vim/apticron
    ansible.builtin.include: roles/vim-apticron/tasks/main.yml

  - name: Install node exporter
    ansible.builtin.include: roles/node-exporter/tasks/main.yml

  - name: Reset account passwords & reboot
    ansible.builtin.include: roles/ansible-post-install/tasks/reboot.yml

  - name: Deploy MOTD
    ansible.builtin.include: roles/motd/deploy.yml
```
